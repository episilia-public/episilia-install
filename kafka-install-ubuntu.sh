#!/bin/bash

function terminate {
	echo "ERROR : failed to execute command :: \"$@\""
	echo
	exit 1
}

function validate {
        if ! $(exit $1); then
		terminate "${@:2}"
        fi
}

function update_apt {
	echo "updating apt repo"
	cmd="sudo apt update -y"
	run=`$cmd 2> /dev/null`
        validate $? $cmd
}

function install_package {
	echo "installing $1 package"
	cmd="sudo apt install $1 -y"
	run=`$cmd 2> /dev/null`
        validate $? $cmd
}

function apply_service {
	if [[ $1 == "start" ]]; then
		echo "starting $2 service"
	elif [[ $1 == "restart" ]]; then
		echo "re-starting $2 service"
	elif [[ $1 == "enable" ]]; then
                echo "enabling $2 service on start-up"
	fi
	cmd="sudo systemctl $1 $2"
	$cmd
	validate $? $cmd
}

function update_ufw {
	echo "updating ufw rules"
	do=`sudo ufw allow ssh`
	do=`sudo ufw --force enable`
	echo "opening port TCP 9092"
	do=`ufw allow 9092/tcp`
	if [[ $1 == "kafka" ]]; then
		echo "opening port TCP 2181"
		do=`ufw allow 2181/tcp`
	fi
}

function get_kafka {
	last=`echo "${1: -1}"`
	if [[ $last == "/" ]]; then
		dir=$1
	else
		dir=$1/
	fi
	echo "downloading kafka binaries"
        get=`curl https://archive.apache.org/dist/kafka/2.6.2/kafka_2.12-2.6.2.tgz -o ${dir}kafka.tgz`
	validate $? "to download kafka binaries"
	echo "extracting kafka binaries"
        extract=`tar -xvzf ${dir}kafka.tgz -C $dir`
	validate $? "to extrct binaries"
	mv ${dir}kafka_2.12-2.6.2 ${dir}kafka
	rm ${dir}kafka.tgz
}

function create_kafka_service {
	echo "creating zookeeper.service (/etc/systemd/system/zookeeper.service)"
	sudo tee /etc/systemd/system/zookeeper.service > /dev/null << END
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=root
ExecStart=${dir}kafka/bin/zookeeper-server-start.sh ${dir}kafka/config/zookeeper.properties
ExecStop=${dir}kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
END
	validate $? "to create zookeeper.service"
	echo "creating kafka.service (/etc/systemd/system/kafka.service)"
	sudo tee /etc/systemd/system/kafka.service > /dev/null << END
[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=root
ExecStart=/bin/sh -c '${dir}kafka/bin/kafka-server-start.sh ${dir}kafka/config/server.properties > ${dir}kafka/kafka.log 2>&1'
ExecStop=${dir}kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
END
	validate $? "to create kafka.service"
}

function update_kafka {
	line=`grep -n '#listeners' ${dir}kafka/config/server.properties | cut -d ':' -f1`
	sed -i "${line}s/.*/listeners=PLAINTEXT:\/\/${1}:9092/" ${dir}kafka/config/server.properties
	line=`grep -n '#advertised.listeners' ${dir}kafka/config/server.properties | cut -d ':' -f1`
	sed -i "${line}s/.*/advertised.listeners=PLAINTEXT:\/\/${1}:9092/" ${dir}kafka/config/server.properties
	create_kafka_service
}

function kafka_topic {
	create=`${dir}kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic $1`
}

function redpanda_topic {
	create=`rpk topic create $1`
}

function service_status {
	while true
        do
		sleep 3
                cmd=`sudo systemctl status $1`
		status=$?
		if ! [[ $status ]]; then
			echo "bringing-up $1 service"
			continue
		else
			echo "$1 service is up"
			break
		fi
	done
}

function create_topic {
	service_status $1
	topic_list=${@:2}
	for topic in ${topic_list[@]}
	do
		echo "creating topic $topic"
		if [[ $1 == "kafka" ]]; then
			kafka_topic $topic
		elif [[ $1 == "redpanda" ]]; then
			redpanda_topic $topic
		fi
	done
}

function success {
	echo "Successfully installed & started $1 service ==> $2:9092"
	echo
}

echo
echo "1. Apache Kafka"
echo "2. Redpanda"
read -p "select a service to proceed (default \"apache kafka\") : " choice < /dev/tty
echo
read -p "enter topics to be created : " -ra topics < /dev/tty
echo
read -p "enter public ip to expose above service (default \"localhost\"): " public_ip < /dev/tty
if [ -z "$public_ip" ];then public_ip="localhost";fi
export service_public_ip=$public_ip
echo

if [[ $choice == 2 ]]
then
	echo "########## Installing  Redpanda ##########"
	curl -1sLf 'https://packages.vectorized.io/nzc4ZYQK3WRGd9sy/redpanda/cfg/setup/bash.deb.sh' | sudo -E bash
	update_apt
	install_package "redpanda"
	update_ufw "redpanda"
	apply_service "start" "redpanda"
	apply_service "enable" "redpanda"
	python3 -c 'import os,yaml; file = open("/etc/redpanda/redpanda.yaml"); data = yaml.safe_load(file); data["redpanda"]["advertised_kafka_api"] = [{"address": os.environ["service_public_ip"],"port": "9092"}]; data["redpanda"]["kafka_api_tls"] = [{}]; file = open("/etc/redpanda/redpanda.yaml","w"); data = yaml.dump(data); file.write(data); file.close()'
	status=$?
	validate $status "to update /etc/redpanda/redpanda.yaml file"
	apply_service "restart" "redpanda"
	create_topic "redpanda" ${topics[@]}
	success "Redpanda" "$public_ip"
else
	read -p "enter a directory to download kafka binary (default /root/) : " kafka_dir < /dev/tty
	if [ -z "$kafka_dir" ];then kafka_dir="/root/";fi
	echo
	echo "########## Installing  Apache Kafka ##########"
	update_apt
        install_package "default-jdk"
	get_kafka $kafka_dir
	update_ufw "kafka"
	update_kafka $public_ip
	apply_service "start" "kafka"
        apply_service "enable" "kafka"
	apply_service "enable" "zookeeper"
	create_topic "kafka" ${topics[@]}
	success "Apache Kafka" "$public_ip"
fi
