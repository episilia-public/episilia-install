Episilia 0.6.1 Release Notes
============================

Features and Enhancements ::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 No enhancements


Bug fixes ::
~~~~~~~~~~~~~
  The NF for basic_string::_M_create which is used to allocate memory to string has been fixed

Config Changes::
~~~~~~~~~~~~~~~~~
 There are no config changes